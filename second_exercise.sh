#!/bin/bash

find ~/ -name '*.log' | sed 's/\ /\\ /' | xargs grep error -l
